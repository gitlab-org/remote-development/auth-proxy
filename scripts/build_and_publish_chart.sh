#!/usr/bin/env bash

# This script build and publishes the helm chart
#
# It uses the following arguments
# USER - user for authenticating with package registry.
# TOKEN - password for authenticating with package registry.
# CHART_NAME - chart name.
# CHART_VERSION - chart version.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

USER=$1
TOKEN=$2
CHART_NAME=$3
CHART_VERSION=$4

if [ -z "${USER}" ]; then
    echo "USER is not set"
    exit 1
fi

if [ -z "${TOKEN}" ]; then
    echo "TOKEN is not set"
    exit 1
fi

if [ -z "${CHART_NAME}" ]; then
    CHART_NAME="gitlab-workspaces-proxy"
    echo "CHART_NAME is not set. Using '${CHART_NAME}'"
fi

if [ -z "${CHART_VERSION}" ]; then
    CHART_VERSION="dev-$(TZ=UTC date '+%Y%m%d%H%M%S')"
    echo "CHART_VERSION is not set. Using '${CHART_VERSION}'"
fi

echo "Packaging helm chart"
helm package ./helm

PROJECT_PATH="${CI_PROJECT_PATH:-gitlab-org/remote-development/gitlab-workspaces-proxy}"
URL_ENCODED_PROJECT_PATH=$(echo "${PROJECT_PATH}" | jq -Rr @uri)
URL="https://gitlab.com/api/v4/projects/${URL_ENCODED_PROJECT_PATH}/packages/helm/api/devel/charts"

echo "Publishing helm chart => ${CHART_NAME}-${CHART_VERSION}"
curl --fail-with-body --request POST \
    --form "chart=@${CHART_NAME}-${CHART_VERSION}.tgz" \
    --user ${USER}:${TOKEN} \
    "${URL}"
