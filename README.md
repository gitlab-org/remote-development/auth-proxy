# gitlab-workspaces-proxy

:warning: Project has been moved to https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-proxy :warning:

With `gitlab-workspaces-proxy`, you can authenticate and authorize the [workspaces](https://docs.gitlab.com/ee/user/workspace/) running in the cluster.
The gitlab-workspaces-proxy uses a central proxy design and automatically discovers backends based on annotations on the Kubernetes service.

## Release Process

- Update the container image version if there are any changes to the application code. The value would be the new git tag which will be pushed.
  - `appVersion` in `helm/Chart.yaml`.
- Update the Helm chart version if there are any changes to the helm chart. The value will be different from the git tag that will be pushed.
  - `version` in `helm/Chart.yaml`.
- Create and push a new git tag.
    ```
    # update the TAG_NAME
    TAG_NAME=0.0
    git tag -a "${TAG_NAME}" -m "version ${TAG_NAME}"
    git push origin "${TAG_NAME}"
    ```

## Local Development

If you are making changes to this codebase, run the following commands.

```shell
# Add `sample_config` with updated `client_id` and `client_secret`
cat <<EOT >> sample_config.yaml
auth:
  client_id: ""
  client_secret: ""
  host: http://gdk.test:3000
  redirect_uri: http://127.0.0.1:9876/auth/callback
  signing_key: passwordpassword
  protocol: http
port: 9876
metrics_path: "/metrics"
EOT

# run
make

# docker build - update USER and TOKEN
USER="UPDATE_ME" TOKEN="UPDATE_ME" IMAGE_NAME="" IMAGE_VERSION="" make docker-build-and-publish

# helm package - update USER and TOKEN
USER="UPDATE_ME" TOKEN="UPDATE_ME" CHART_NAME="" CHART_VERSION="" make helm-build-and-publish
```

## Local Installation

This section outlines steps to get gitlab-workspaces-proxy running on a local Kubernetes cluster for local development and testing purposes.
Refer our [docs](https://docs.gitlab.com/ee/user/workspace/set_up_workspaces_proxy.html) for steps to install gitlab-workspaces-proxy in a production environment.

**Prerequisites**:

- Your Kubernetes cluster must be running.
- You must have an Ingress controller installed.
- You must have `kubectl` and `helm` 3.11.0 and later on your local machine.

To install the gitlab-workspaces-proxy, you must complete the following steps.

### Step 1: Generate TLS certificates

You must generate TLS certificates using [`mkcert`](https://github.com/FiloSottile/mkcert) for:

- The domain that `gitlab-workspaces-proxy` listens on (`GITLAB_WORKSPACES_PROXY_DOMAIN`).
- The domain that workspaces are available on (`GITLAB_WORKSPACES_WILDCARD_DOMAIN`).

```sh
brew install mkcert
brew install nss

export GITLAB_WORKSPACES_PROXY_DOMAIN="workspaces.localdev.me"
export GITLAB_WORKSPACES_WILDCARD_DOMAIN="*.workspaces.localdev.me"
mkcert -install
mkcert "${GITLAB_WORKSPACES_PROXY_DOMAIN}" "${GITLAB_WORKSPACES_WILDCARD_DOMAIN}"

export WORKSPACES_DOMAIN_CERT="${PWD}/workspaces.localdev.me+1.pem"
export WORKSPACES_DOMAIN_KEY="${PWD}/workspaces.localdev.me+1-key.pem"
export WILDCARD_DOMAIN_CERT="${PWD}/workspaces.localdev.me+1.pem"
export WILDCARD_DOMAIN_KEY="${PWD}/workspaces.localdev.me+1-key.pem"
```

### Step 2: Register an app on your GitLab instance

1. [Configure GitLab as an OAuth 2.0 authentication identity provider](https://docs.gitlab.com/ee/integration/oauth_provider.html).
1. Set the redirect URI to `https://${GITLAB_WORKSPACES_PROXY_DOMAIN}/auth/callback`.
1. Set the scopes to `api`, `read_user`, `openid`, and `profile`.
1. Store the client ID and generated secret in a secret vault (for example, 1Password).

```sh
export CLIENT_ID="your_application_id"
export CLIENT_SECRET="your_application_secret"
export REDIRECT_URI="https://${GITLAB_WORKSPACES_PROXY_DOMAIN}/auth/callback"
```

### Step 3: Generate SSH host keys

In this example, let's generate an RSA key.
You can also generate an ECDSA key instead.

```sh
ssh-keygen -f ssh-host-key -N '' -t rsa
export SSH_HOST_KEY=$(pwd)/ssh-host-key
```

### Step 4: Export more environment variable

```sh
export GITLAB_URL="http://gdk.test:3000"
export SIGNING_KEY="make_up_a_random_key_consisting_of_letters_numbers_and_special_chars"
```

### Step 5: Install helm chart

Install the Helm chart.
Modify the `ingress.className` parameter if you're using a different Ingress class.
The default value is `nginx`.

```sh
helm repo add gitlab-workspaces-proxy \
  https://gitlab.com/api/v4/projects/gitlab-org%2fremote-development%2fgitlab-workspaces-proxy/packages/helm/devel

helm repo update

helm upgrade --install gitlab-workspaces-proxy \
  gitlab-workspaces-proxy/gitlab-workspaces-proxy \
  --version 0.1.13 \
  --namespace=gitlab-workspaces \
  --create-namespace \
  --set="auth.client_id=${CLIENT_ID}" \
  --set="auth.client_secret=${CLIENT_SECRET}" \
  --set="auth.host=${GITLAB_URL}" \
  --set="auth.redirect_uri=${REDIRECT_URI}" \
  --set="auth.signing_key=${SIGNING_KEY}" \
  --set="ingress.host.workspaceDomain=${GITLAB_WORKSPACES_PROXY_DOMAIN}" \
  --set="ingress.host.wildcardDomain=${GITLAB_WORKSPACES_WILDCARD_DOMAIN}" \
  --set="ingress.tls.workspaceDomainCert=$(cat ${WORKSPACES_DOMAIN_CERT})" \
  --set="ingress.tls.workspaceDomainKey=$(cat ${WORKSPACES_DOMAIN_KEY})" \
  --set="ingress.tls.wildcardDomainCert=$(cat ${WILDCARD_DOMAIN_CERT})" \
  --set="ingress.tls.wildcardDomainKey=$(cat ${WILDCARD_DOMAIN_KEY})" \
  --set="ssh.host_key=$(cat ${SSH_HOST_KEY})" \
  --set="ingress.className=nginx"
```

### Step 6: Verification of Kubernetes resources created

1.  Verify the created secret has correct configuration.

    ```sh
    kubectl -n gitlab-workspaces get secret gitlab-workspaces-proxy  -o=go-template='{{index .data "config.yaml"}}' | base64 -d
    ```

2.  Verify the created `Ingress` resource for the `gitlab-workspace` namespace:

    ```sh
    kubectl -n gitlab-workspaces get ingress
    ```

3.  Verify that the pods are in the `Running` state

    ```sh
    kubectl -n gitlab-workspaces get pods
    ```

If you deploy the GitLab Helm chart to any namespace other than the default `gitlab-workspaces`,
update the namespace in the GitLab agent configuration:

```yaml
remote_development:
  gitlab_workspaces_proxy:
    namespace: "<custom-gitlab-workspaces-proxy-namespace>"
```

### Step 7: Verification

1. To test if `gitlab-workspaces-proxy` is accessible,
   run an external `curl` command to inspect the gitlab-workspaces-proxy pod logs:

    ```sh
    curl -vL ${GITLAB_WORKSPACES_PROXY_DOMAIN}
    ```

    This command returns a `400 Bad Request` error. Workspaces are authorized when you create them in GitLab.

1. In another terminal, run the following command:

    ```sh
    kubectl -n gitlab-workspaces logs -f -l app.kubernetes.io/name=gitlab-workspaces-proxy
    ```

    The logs show a `could not find upstream workspace upstream not found` error.
