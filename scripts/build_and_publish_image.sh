#!/usr/bin/env bash

# This script build and publishes the docker image
#
# It uses the following arguments
# USER - user for authenticating with container registry.
# TOKEN - password for authenticating with container registry.
# IMAGE_NAME - container image name.
# IMAGE_VERSION - container image version.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

USER=$1
TOKEN=$2
IMAGE_NAME=$3
IMAGE_VERSION=$4

if [ -z "${USER}" ]; then
    echo "USER is not set"
    exit 1
fi

if [ -z "${TOKEN}" ]; then
    echo "TOKEN is not set"
    exit 1
fi

if [ -z "${IMAGE_NAME}" ]; then
    IMAGE_NAME="registry.gitlab.com/gitlab-org/remote-development/gitlab-workspaces-proxy"
    echo "IMAGE_NAME is not set. Using '${IMAGE_NAME}'"
fi

if [ -z "${IMAGE_VERSION}" ]; then
    IMAGE_VERSION="dev-$(TZ=UTC date '+%Y%m%d%H%M%S')"
    echo "IMAGE_VERSION is not set. Using '${IMAGE_VERSION}'"
fi

echo "Building container image => ${IMAGE_NAME}:${IMAGE_VERSION}"
docker build --platform=linux/amd64 -t "${IMAGE_NAME}:${IMAGE_VERSION}" -f ./Dockerfile .
echo "Logging into container registry => ${CI_REGISTRY}"
docker login -u "${USER}" -p "${TOKEN}" "${CI_REGISTRY}"
echo "Publishing container image => ${IMAGE_NAME}:${IMAGE_VERSION}"
docker push "${IMAGE_NAME}:${IMAGE_VERSION}"
